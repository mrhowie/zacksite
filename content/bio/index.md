+++
title = 'Bio'
date = 2023-09-19T23:11:13-07:00
draft = false
+++
Official Bio:  
  
Vancouverite Isaac Howie has always loved making music. A student of piano and composition since early childhood, his musical career has included founding a jazz trio; composing and performing for concert bands; publishing electronic music; and winning awards for solo piano. He entered the University of British Columbia at 15, where he currently studies organ under the mentorship of Professor Michael Dirk. Alongside his pursuit of a BMus, Isaac is Organist at Holy Family Parish; he holds an ARCT in piano and studies harpsichord with Alex Weimann. In 2021, Isaac was division winner of the Vancouver Chamber Choir's Young Composers' Competition, and in 2023, he was a semifinalist in the RCCO National Organ Competition. In his spare time, Isaac enjoys building computers and translating Ancient Greek texts. (08/23)
